# Fakedate

[Fakedate](https://gitlab.com/ufoot/fakedate) generates pseudo-random dates and times.

It is written in [Rust](https://www.rust-lang.org/)
and is mostly a toy project to ramp up on the language.
It might however be useful. Use at your own risk.

![Fakedate icon](https://gitlab.com/ufoot/fakedate/raw/master/fakedate.png)

# Status

[![Build Status](https://gitlab.com/ufoot/fakedate/badges/master/pipeline.svg)](https://gitlab.com/ufoot/fakedate/pipelines)

Current version is 0.1.2.

# Install

No install target yet, copy the `fakedate` binary in your `$PATH` if you wish, that's all.

A few commands which may prove useful:

```sh
cargo build             # build debug binary in ./target/debug/
cargo build --release   # build release binary in ./target/release/
cargo test              # launch tests
rustfmt src/*.rs        # format code
./docker-build.sh       # build Docker image with version tag
./bump-version.sh       # bump minor version number
```

# Usage

Simply launch:

```sh
cargo run
```

Or just run the binary directly:

```sh
./target/debug/fakedate
./target/release/fakedate
```

Alternatively, using docker:

```sh
docker run ufoot/fakedate
```

To pass options:

```sh
cargo run -- -1975
```

By default, the date is a random date for the current year.
To use a different year:

```sh
fakedate -1975
```

Other standard options include:

* `-n`, `--no-newline`: no newline char at the end of the date
* `-w`, `--week-end`: force the date to be last week-end
* `-h`, `--help`: display a short help.
* `-v`, `--version`: display version.

# Legal information

Fakedate is licensed under the [MIT](https://gitlab.com/ufoot/fakedate/blob/master/LICENSE) license.
