#!/bin/sh

# Small utility to update version number.
# Source of truth is considered to be Cargo.toml.

SRC=./src
VERSION=$(cat ./Cargo.toml | grep "^version" | sed "s/^version\s*=//g" | sed "s/\\\"//g" | sed "s/\ //g")
MAJOR=$(echo $VERSION | sed "s/\([0-9]*\)\.[0-9]*\.[0-9]*-[0-9a-z]*/\1/g")
MINOR=$(echo $VERSION | sed "s/[0-9]*\.\([0-9]*\)\.[0-9]*-[0-9a-z]*/\1/g")
OLD_REVISION=$(echo $VERSION | sed "s/[0-9]*\.[0-9]*\.\([0-9]*\)-[0-9a-z]*/\1/g")
OLD_GIT_COMMIT=$(echo $VERSION | sed "s/[0-9]*\.[0-9]*\.[0-9]*-\([0-9a-z]*\)/\1/g")
NEW_REVISION=$(($OLD_REVISION+1))
NEW_GIT_COMMIT=$(git log --oneline -- $SRC | head -n 1 | cut -c 1-7)

if test "$OLD_GIT_COMMIT" == "$NEW_GIT_COMMIT" ; then
	echo "no new commit in $SRC"
	echo "$MAJOR.$MINOR.$OLD_REVISION-$OLD_GIT_COMMIT"
	exit 0
else
	echo "new commits in $SRC"
	echo "$MAJOR.$MINOR.$OLD_REVISION-$OLD_GIT_COMMIT -> $MAJOR.$MINOR.$NEW_REVISION-$NEW_GIT_COMMIT"
fi

sed -i "s/$MAJOR\.$MINOR\.$OLD_REVISION-$OLD_GIT_COMMIT/$MAJOR.$MINOR.$NEW_REVISION-$NEW_GIT_COMMIT/g" ./Cargo.toml

for f in ./Dockerfile ./docker-build.sh ./README.md  ; do
	sed -i "s/$MAJOR\.$MINOR\.$OLD_REVISION/$MAJOR.$MINOR.$NEW_REVISION/g" $f
done

cargo test
