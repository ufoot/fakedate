use fakedate::options;
use fakedate::run;

fn main() {
    let cfg = options::Config::parse();
    run(&cfg);
}
